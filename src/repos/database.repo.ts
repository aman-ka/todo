/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import {paginationType} from '../interfaces';

const getAll = async (
  dbEntity:any,
  recordLimit:number,
  pageNumber:number):Promise<paginationType> =>{
  const record =  await dbEntity.find({})
  .sort( { _id: 1 } )
  .skip( pageNumber > 0 ? ( ( pageNumber - 1 ) * recordLimit ) : 0 )
  .limit(recordLimit);
  return ({
    data:record,
    numberOfRecords:recordLimit,
    pageNumber:pageNumber,
  });
}

const bulkUpdate = async (dbEntity:any,filter:object,payload:{task:string}):Promise<any> =>{
  return await dbEntity.update(filter,payload).exec();
}

const bulkDelete = async (dbEntity:any,filter:object):Promise<any> =>{
  return await dbEntity.deleteMany(filter).exec();
}

const findById = async (dbEntity:any,id:string):Promise<any> =>{
  return await dbEntity.findById(id).exec();
}

const findByIdAndDelete = async (dbEntity:any,id:string):Promise<any> =>{
  return await dbEntity.findByIdAndDelete(id).exec();
}

const findByIdAndUpdate = async (dbEntity:any,id:string,payload:{task:string}):Promise<any> =>{
  return await dbEntity.findByIdAndUpdate(id,payload).exec();
}

const insert = async (dbEntity:any,payload:{task:string}):Promise<any> =>{
    return await dbEntity.create(payload);
  }

export {
    getAll,
    findByIdAndUpdate,
    findByIdAndDelete,
    findById,
    bulkDelete,
    bulkUpdate,
    insert,
};