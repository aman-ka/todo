/* eslint-disable @typescript-eslint/no-unsafe-call */
import {dbrepo} from '@repos/index';
const {getAll,insert} = dbrepo;
/**
 * Get all users.
 * 
 * @returns 
 */
function getAllTasks(dbEntity:any,recordLimit:number,pageNumber:number): Promise<any> {
    return getAll(dbEntity,recordLimit,pageNumber);
}

function insertTask(dbEntity:any,payload:{task:string}): Promise<any> {
    return insert(dbEntity,payload);
}

// Export default
export{
    getAllTasks,
    insertTask,
};
