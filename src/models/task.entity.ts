/* eslint-disable @typescript-eslint/no-unsafe-call */
import { model, Schema, Model} from 'mongoose';
import {taskType} from '../interfaces';

const TaskSchema: Schema = new Schema({
  task: { type: String, required: true },
  date: { type: Date,default: Date.now},
});

const TaskModel: Model<taskType> = model('Task', TaskSchema);

export {TaskModel};