export interface IDataRecordPagination {
    data: object,
    numberOfRecords: number,
    pageNumber: number
}
