/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import StatusCodes from 'http-status-codes';
import { Request, Response, Router } from 'express';

import {TaskModel} from '@models/index';

import {taskService} from '@services/index';
 const {getAllTasks,insertTask} = taskService;


// Constants
const router = Router();
const { CREATED, OK } = StatusCodes;

// Paths
export const p = {
    get: '/all',
    post: '/add'
} as const;



/**
 * Get all task.
 */
router.get(p.get, (_: Request, res: Response) => {
    (async () =>{
    const tasks = await getAllTasks(TaskModel,Number(_.query.pageNumber),Number(_.query.limit));
    return res.status(OK).json({tasks});
    })();
});

/**
 * Add task.
 */
 router.post(p.post, (_: Request, res: Response) => {
    (async () =>{
    const body = _.body;
    const tasks = await insertTask(TaskModel,body);
    return res.status(OK).json({tasks});
    })();
});

// Export default
export default router;
