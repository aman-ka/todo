import { Router } from 'express';
import taskRouter from './task-router';


// Export the base-router
const baseRouter = Router();

// Setup routers
baseRouter.use('/tasks', taskRouter);

// Export default.
export default baseRouter;
